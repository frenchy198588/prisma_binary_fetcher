import subprocess
import json
import sys

def build_url(base, channel, version, platform, binary, extension):
    return "https://{base}/{channel}/{version}/{platform}/{binary}.{extension}".format(
        base=base, channel=channel,
        version=version, platform=platform,
        binary=binary, extension=extension)

def download_for_version(prisma_version, version):
    # For centos and for ubuntu 18 onwards
    oss = ["rhel-openssl-1.0.x", "debian-openssl-1.1.x"]
    files = ["query-engine", "introspection-engine", "prisma-fmt", "migration-engine"]
    extension = "gz"
    base = "binaries.prisma.sh"
    channel = "all_commits"

    proc = subprocess.Popen("mkdir bin", shell=True)
    out, err = proc.communicate()
    
    proc = subprocess.Popen("mkdir bin/{pv}".format(pv=prisma_version), shell=True)
    out, err = proc.communicate()

    for os in oss:
        proc = subprocess.Popen("mkdir bin/{pv}/{os}".format(pv=prisma_version, os=os), shell=True)
        out, err = proc.communicate()

    for file in files:
        for os in oss:
            file_url = build_url(base, channel, version, os, file, extension)
            proc = subprocess.Popen("wget -o /dev/null {file_url} -P bin/{directory}/{os}".format(file_url=file_url, directory=prisma_version, os=os), shell=True)

def run_npm(prisma_version):
    print('npm install prisma@{version} --force'.format(version=prisma_version))
    proc = subprocess.Popen('npm install prisma@{version} --force'.format(version=prisma_version), shell=True)
    out, err = proc.communicate()
    print("NPM has downloaded")
    with open("./node_modules/@prisma/engines/package.json") as packagejson:
        packagejson_json = json.load(packagejson)
        version = packagejson_json["version"]
        engine_version = version.split(".")[-1]
        return engine_version

def clean_dir():
    subprocess.Popen("rm -r ./bin", shell=True).communicate()
    subprocess.Popen("rm -r ./node_modules", shell=True).communicate()
    subprocess.Popen("rm package-lock.json", shell=True).communicate()
    subprocess.Popen("rm package.json", shell=True).communicate()

    # Reinit npm to have a fresh package.json
    subprocess.Popen("npm init -y", shell=True).communicate()

def package(prisma_version):
    subprocess.Popen("tar -czf bundle-{prisma_version}.tgz bin".format(prisma_version=prisma_version), shell=True).communicate()

def main():
    print("Starting downloader")
    prisma_version = sys.argv[1]
    print("Downloading dependencies for prisma {prisma_version}".format(prisma_version=prisma_version))

    clean_dir()

    engine_version = run_npm(prisma_version)
    download_for_version(prisma_version, engine_version)

    package(prisma_version)

if __name__ == "__main__":
    main()
